class Post < ActiveRecord::Base
  # Hello world
  has_many :comments, dependent: :destroy
  validates_presence_of :title
  validates_presence_of :body
end
